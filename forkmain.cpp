
#include <iostream>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <ctime>

#include "forkyoutube.h"

using namespace std;

int main(int argc, char **argv){
    if(argc!=2){
        cout << "Error, ejecute correctamente el programa." << endl;
        exit(-1);
    } else{
        // Punteros que guardan url y nombre del video.
        char *inurl, *innom;

        // Se guarda url en argv[1].
        inurl = argv[1];

        // Se da nombre al archivo descargado.
        char nom[10];
        srand(time(NULL));
        for(int i = 0; i < 6; i++){
            nom[i] = 64+rand()%(59);
        }
        nom[6] = '.';
        nom[7] = 'm';
        nom[8] = 'p';
        nom[9] = '3';
        cout << "Archivo: " << nom << endl;
        cout << endl;
        innom = nom;

        Fork  miFork(1, inurl, innom);

    }
    return 0;
}