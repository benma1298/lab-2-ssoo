prefix=/usr/local
CC = g++

CFLAGS = -g -Wall
SRC = forkmain.cpp forkyoutube.cpp
OBJ = forkmain.o forkyoutube.o
APP = forkmain

all: $(OBJ)
	$(CC) $(CFLAGS) -o $(APP) $(OBJ)

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
