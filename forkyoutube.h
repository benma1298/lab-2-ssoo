#ifndef FORKYOUTUBE_H
#define FORKYOUTUBE_H

#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

using namespace std;

class Fork{
    private:
    pid_t pid;
    int seconds;
    char *name;
    char *URL;

    public:
    Fork(int s, char *url, char *nom);
    void create();
    void execute();
};

#endif