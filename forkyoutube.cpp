
#include <iostream>
#include <unistd.h>
#include <sys/wait.h>

#include "forkyoutube.h"

using namespace std;

// Creacion del Fork.
Fork::Fork(int s, char *url , char *nom){
    name = nom;
    seconds = s;
    URL = url;
    create();
    execute();
}

// Se crea el proceso hijo.
void Fork::create(){
    pid = fork();
}

void Fork::execute(){
    // Validacion del proceso hijo.
    if(pid < 0){
        cout << "No se pudo crear el proceso ...";
        cout << endl;

    // Codigo del proceso hijo.
    } else if(pid == 0){
        
        cout << "ID Proceso hijo: " << getpid();
        cout << endl << endl;
        cout << "Ejecuta código proceso hijo ...";
        cout << endl;
        cout << "Descargando video y extrayendo audio ..." << endl;
        execlp("youtube-dl", "youtube-dl", "-x", "--restrict-filenames", "--output", name, "--audio-format", "mp3", URL, NULL);
        cout << endl;
        sleep(seconds);
    } else {
        // Proceso padre espera el termino del proceso hijo.
        wait(NULL);
        cout << "Termina código de proceso hijo ...";
        cout << endl;
        cout << "Continua con código proceso padre: " << getpid();
        cout << endl;
        cout << "\n Reproduciendo cancion ..." << endl;
        execlp("cvlc", "cvlc", name, NULL);
        cout << endl;
    }
}